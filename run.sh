#!/usr/bin/env bash

# Filename: run.sh
# Description: This script can be configured using environment variables or command line arguments (or a combination).
#              The script launches a file monitor to watch the directory specified and POSTs event notifications
#              to the URL specified.  It will check the file paths against a list of regular expressions so that some
#              files can be ignored.
#
#              The format of the command line argument call is:
#
#              ./run.sh \
#                  -d directory-to-monitor \
#                  -m monitor-method \
#                  -n fswatch-monitor \
#                  -u base-url \
#                  -r repo-name \
#                  -o log-level \
#                  -f filter-patterns \
#                  -e endpoint-urls
#
#              where:
#              directory-to-monitor: This is the directory that will be monitored
#              monitor-method: Is the executable to use to monitor, either 'fswatch' or 'inotify' (default)
#              fswatch-monitor: (optional) Is the monitor that you want to use while using fswatch.  This overrides the default. Consult the docs for fswatch.
#              base-url: This is the base part of the HTTP url that points to directory-to-monitor
#              repo-name: This is an arbitrary name you can give to this data directory (so receiver will have context)
#              log-level: is the 'level' of detail to print in the logs and is either INFO or DEBUG
#              filter-patterns: is a comma separated list of regexps that will be used to exclude certain paths (directories or files)
#              endpoint-urls: this is a comma separated list of URLs of the services that messages will be sent to via HTTP POST
#
#              or environment variables can be set which map to:
#              FSMON_DIRECTORY_TO_MONITOR = directory-to-monitor
#              FSMON_MONITOR_METHOD = monitor-method
#              FSMON_FSWATCH_MONITOR = fswatch-monitor
#              FSMON_BASE_URL = base-url
#              FSMON_REPO_NAME = repo-name
#              FSMON_LOG_LEVEL = log-level
#              FSMON_FILTER_PATTERNS = filter-patterns
#              FSMON_ENDPOINT_URLS = endpoint-url

# Grab all the command line arguments
while getopts ":d:b:u:r:f:m:" o; do
    case "${o}" in
        d)
            FSMON_DIRECTORY_TO_MONITOR=${OPTARG}
            ;;
        m)
            FSMON_MONITOR_METHOD=${OPTARG}
            ;;
        n)
            FSMON_FSWATCH_MONITOR=${OPTARG}
            ;;
        u)
            FSMON_BASE_URL=${OPTARG}
            ;;
        r)
            FSMON_REPO_NAME=${OPTARG}
            ;;
        o)
            FSMON_LOG_LEVEL=${OPTARG}
            ;;
        f)
            FSMON_FILTER_PATTERNS=${OPTARG}
            ;;
        e)
            FSMON_ENDPOINT_URLS=${OPTARG}
            ;;
        *)
            ;;
    esac
done
shift $((OPTIND-1))

# Define a function that can be used to log messages
function log() {
  echo `date +"%Y-%m-%d %T"` "$1"
}

# Look for a directory to monitor and if not set, specify a default of /data/repo
if [[ -z ${FSMON_DIRECTORY_TO_MONITOR} ]]
then
  FSMON_DIRECTORY_TO_MONITOR=/data/repo
fi

# Let's make sure the directory is an absolute path so that this script sends the full path to the next steps.
# If it appears to be a relative path, add the current directory before it to try and make it fully qualified
if [[ ${FSMON_DIRECTORY_TO_MONITOR} = /* ]]
then
  DIR_TO_MONITOR_FULL_PATH=${FSMON_DIRECTORY_TO_MONITOR}
else
  DIR_TO_MONITOR_FULL_PATH=$PWD
  DIR_TO_MONITOR_FULL_PATH+=/
  DIR_TO_MONITOR_FULL_PATH+=${FSMON_DIRECTORY_TO_MONITOR}
fi

# Now check for the repo-name as this is necessary. If one does not exist, create one using the host and dir name
if [[ -z ${FSMON_REPO_NAME} ]]
then
  # Since nothing was specified, grab the directory name and replace spaces with dashes and make lowercase
  DIR_NAME=$(basename "$FSMON_DIRECTORY_TO_MONITOR")
  DIR_NAME=${DIR_NAME,,}
  DIR_NAME=${DIR_NAME// /-}

  # Create a default name using hostname and directory
  FSMON_REPO_NAME=${HOSTNAME}-${DIR_NAME}
  log "[INFO] You need to specify an arbitrary name that will be associated with the files"
  log "[INFO] that are being monitored. This is so the server receiving the messages will know what"
  log "[INFO] repository the consumers should be working with. This should be an all lower case name"
  log "[INFO] with dashes, but no spaces or special characters (e.g. my-repo-name) and can be specified"
  log "[INFO] using the -r flag or the FSMON_REPO_NAME environment variable.  Will use a default of"
  log "[INFO] ${FSMON_REPO_NAME}"
fi

# Let's make sure the directory that will be monitored exists
if ! [[ -e ${DIR_TO_MONITOR_FULL_PATH} ]]
then
  log "[ERROR] The directory specified to monitor, ${DIR_TO_MONITOR_FULL_PATH}, does not exist, exiting..."
  exit 0
fi

# Make sure the directory specified is actually a directory and not a file
if ! [[ -d ${DIR_TO_MONITOR_FULL_PATH} ]]
then
  log "[ERROR] The path specified to monitor, ${DIR_TO_MONITOR_FULL_PATH}, is not a directory, exiting..."
  exit 0
fi

# Log the directory that will be monitored
log "[INFO] This script will monitor ${DIR_TO_MONITOR_FULL_PATH}"

# Calculate the length of that directory name as we will need it for constructing URLs
DIR_TO_MONITOR_FULL_PATH_LENGTH=${#DIR_TO_MONITOR_FULL_PATH}
if [[ ${FSMON_LOG_LEVEL} == "DEBUG" ]]; then log "[DEBUG] The full path of the directory to monitor is ${DIR_TO_MONITOR_FULL_PATH_LENGTH} characters long"; fi

# Check to see if the level of logging was specified and if not, default to INFO
if [[ -z ${FSMON_LOG_LEVEL} ]]
then
  FSMON_LOG_LEVEL="INFO"
fi

# Now make sure the log level is either INFO or DEBUG
if [[ ${FSMON_LOG_LEVEL} == "INFO" ]] || [[ ${FSMON_LOG_LEVEL} == "DEBUG" ]]
then
  log "[INFO] Log level set to ${FSMON_LOG_LEVEL}"
else
  log "[INFO] Logging level not specified, will set to INFO"
  FSMON_LOG_LEVEL="INFO"
fi

# Check to see if a monitoring method was specified and if not, use inotify as default
if [[ -z ${FSMON_MONITOR_METHOD} ]]
then
  log "[INFO] Monitoring method not specified, will use inotify"
  FSMON_MONITOR_METHOD="inotify"
fi

# Now make sure method specified is either inotify or fswatch
if [[ ${FSMON_MONITOR_METHOD} == "inotify" ]] || [[ ${FSMON_MONITOR_METHOD} == "fswatch" ]]
then
  log "[INFO] Will use ${FSMON_MONITOR_METHOD} to monitor the directory"
else
  log "[INFO] Monitor method not specified correctly, should be inotify or fswatch, will use inotify"
  FSMON_MONITOR_METHOD="inotify"
fi

# Make sure base URL was specified
if [[ -z ${FSMON_BASE_URL} ]]
then
  log "[ERROR] You need to specify the base portion of the URL that points to the directory being monitored"
  log "[ERROR] You can do this by either using the -u flag or the FSMON_BASE_URL environment variable, exiting..."
  exit 0
fi

# Now check to see if the endpoint URLs are specified
if [[ -z ${FSMON_ENDPOINT_URLS} ]]
then
  log "[ERROR] You need to specify the URLs where the messages will be sent."
  log "[ERROR] You can do this using either the -e flag or the FSMON_ENDPOINT_URLS environment variable."
  exit 0
fi
log "[INFO] Will send event messages to ${FSMON_ENDPOINT_URLS}"
# TODO kgomes - might want to test the URL??

# Now parse all the URLs into an array
ENDPOINT_URL_ARRAY=()
IFS=',' read -ra ENDPOINT_URL_ARRAY <<< "${FSMON_ENDPOINT_URLS}"

# If there are any filter patterns specified, construct an array of patterns that will be used
# to filter out files from being monitored.
if [[ ${FSMON_LOG_LEVEL} == "DEBUG" ]]; then log "[DEBUG] Filter patterns specified: ${FSMON_FILTER_PATTERNS}"; fi

FILTER_ARRAY=()
if ! [[ -z ${FSMON_FILTER_PATTERNS} ]]
then
  # Parse the comma separated list into an array
  IFS=',' read -ra FILTER_ARRAY <<< "${FSMON_FILTER_PATTERNS}"
fi

# Let's log out the configuration
log "[INFO] FSMON Config:"
log "[INFO] Directory to monitor: ${FSMON_DIRECTORY_TO_MONITOR}"
log "[INFO] Monitoring method: ${FSMON_MONITOR_METHOD}"
log "[INFO] Log file directory: ${FSMON_LOG_FILE_DIRECTORY}"
log "[INFO] Logging level: ${FSMON_LOG_LEVEL}"
log "[INFO] Repo name: ${FSMON_REPO_NAME}"
log "[INFO] Base URL: ${FSMON_BASE_URL}"
log "[INFO] Filter patterns: ${FSMON_FILTER_PATTERNS}"
log "[INFO] Endpoint URL: ${FSMON_ENDPOINT_URL}"
log "[INFO] Endpoint Type: ${FSMON_ENDPOINT_TYPE}"

# This is the function to process the event message and post to a URL
function sendMessage() {

    # OK, so I should have an event that is a comma separated list of base path, event name, filename
    IFS=',' read -r -a array <<< "$1"

    # Start off with the file type being unknown
    RESOURCE_TYPE="UNKNOWN"

    # Grab the parsed values
    TIMESTAMP=${array[0]}
    EVENT=${array[1]}
    FULL_PATH=${array[2]}

    # A couple placeholders for file size and last modification date
    RESOURCE_SIZE=
    RESOURCE_LAST_MODIFICATION_TIMESTAMP=

    # Since different methods can produce different event text, we need to normalize them
    if [[ ${EVENT} == "CREATE" ]]; then EVENT="Created"; fi
    if [[ ${EVENT} == "CREATE:ISDIR" ]]; then EVENT="Created"; fi
    if [[ ${EVENT} == "MODIFY" ]]; then EVENT="Updated"; fi
    if [[ ${EVENT} == "CREATE:ISDIR" ]]; then EVENT="Created"; fi
    if [[ ${EVENT} == "DELETE" ]]; then EVENT="Removed"; fi
    if [[ ${EVENT} == "DELETE:ISDIR" ]]
    then
        EVENT="Removed"
        RESOURCE_TYPE="DIRECTORY"
    fi

    # Check the full path to see if it matches any of the regular expressions to see if it should be skipped
    for pattern in "${FILTER_ARRAY[@]}"
    do
#      if [[ ${FSMON_LOG_LEVEL} == "DEBUG" ]]; then log "[DEBUG] Comparing file path ${FULL_PATH} to regexp ${pattern}"; fi
      if [[ ${FULL_PATH} =~ ${pattern} ]]
      then
        if [[ ${FSMON_LOG_LEVEL} == "DEBUG" ]]; then log "[DEBUG] ${FULL_PATH} matched the regexp ${pattern} so file should be skipped"; fi
        return
      fi
    done

    # Since we have to process the event, double check to make sure the timestamp does not have an equal sign in front
    # which can be the case with fswatch
    if [[ ${TIMESTAMP} =~ ^=.* ]]
    then
      if [[ ${FSMON_LOG_LEVEL} == "DEBUG" ]]; then log "[DEBUG] ${FULL_PATH} matched the regexp ${pattern} so file should be skipped"; fi
      TIMESTAMP=${TIMESTAMP:1}
    fi

    # TODO kgomes - do I have to look for ":" separated events from fswatch?

    # Create the resource URL from the directory and base URL
    RESOURCE_URL="${FSMON_BASE_URL}${FULL_PATH:${DIR_TO_MONITOR_FULL_PATH_LENGTH}}"
    # TODO kgomes - I think I would need to URL encode this (non-base part of the file path)?

    # First thing to do is check to see if the path given even exists
    if [[ -e ${FULL_PATH} ]]
    then
       # Now check to see if it's a directory
       if [[ -d ${FULL_PATH} ]]
       then
          RESOURCE_TYPE="DIRECTORY"
       else
          # Set the resource type
          RESOURCE_TYPE="FILE"

          # Now get the size and last mod date
          RESOURCE_SIZE=$(stat -c%s "${FULL_PATH}")
          RESOURCE_LAST_MODIFICATION_TIMESTAMP=$(stat -c%Y "${FULL_PATH}")
       fi
    fi
    if [[ ${FSMON_LOG_LEVEL} == "DEBUG" ]]; then log "[DEBUG] ${FULL_PATH} is a ${RESOURCE_TYPE} and has a URL of ${RESOURCE_URL}"; fi

    # Now construct message
    MESSAGE_TO_SEND="{\"timestamp\":\"${TIMESTAMP}\",\"event\":\"${EVENT}\",\"repoName\":\"${FSMON_REPO_NAME}\",\"resourceURL\":\"${RESOURCE_URL}\",\"resourceType\":\"${RESOURCE_TYPE}\",\"size\":\"${RESOURCE_SIZE}\",\"modTimestamp\":\"${RESOURCE_LAST_MODIFICATION_TIMESTAMP}\"}"
    if [[ ${FSMON_LOG_LEVEL} == "DEBUG" ]]; then log "[DEBUG] ${MESSAGE_TO_SEND}"; fi

    # Now loop over the URLs where the messages should be sent to
    for ENDPOINT_URL in "${ENDPOINT_URL_ARRAY[@]}"
    do
        if [[ ${FSMON_LOG_LEVEL} == "DEBUG" ]]; then log "[DEBUG] Response: `curl -d "${MESSAGE_TO_SEND}" -k -H 'Content-Type: application/json' ${ENDPOINT_URL}`"; else curl -d "${MESSAGE_TO_SEND}" -k -H 'Content-Type: application/json' ${ENDPOINT_URL} > /dev/null 2>&1; fi
        if [[ ${FSMON_LOG_LEVEL} == "DEBUG" ]]; then log "[DEBUG] Message sent to ${ENDPOINT_URL}"; fi
    done

}

# Start the test server in case it's needed
PROCESS_NUM=$(ps -ef | grep "node server.js" | grep -v "grep" | wc -l)
if [[ ${PROCESS_NUM} -eq 0 ]]
then
    # Start the test server
    log "[INFO] Starting test server ..."
    node /home/fsmon/fsmon-test/server.js &
fi

# Since we have a log directory and repo name, log the start of the run
log "[INFO] Starting to monitor ${DIR_TO_MONITOR_FULL_PATH}"

# Now it's time to actually start up the monitoring.  This will depend on the method chosen
if [[ ${FSMON_MONITOR_METHOD} == "inotify" ]]
then
  log "[INFO] using inotify"
  IFS=''
  inotifywait --timefmt %s --format %T,%:e,%w%f -r -m -e modify,moved_to,moved_from,move,create,delete ${DIR_TO_MONITOR_FULL_PATH} | while read EVENTINFO
  do
    if [[ ${FSMON_LOG_LEVEL} == "DEBUG" ]]; then log "[DEBUG] Received message from inotify: ${EVENTINFO}"; fi
    sendMessage ${EVENTINFO}
  done
fi

# Check to see if the monitoring method is 'fswatch'
if [[ ${FSMON_MONITOR_METHOD} == "fswatch" ]]
then
  # Since we are using fswatch, there is an option to choose which monitor you want to use.  Look for the environment
  # variable that could hold that
  FSWATCH_MONITOR=
  if ! [[ -z ${FSMON_FSWATCH_MONITOR} ]]
  then
    FSWATCH_MONITOR=" -m ${FSMON_FSWATCH_MONITOR}"
    if [[ ${FSMON_LOG_LEVEL} == "DEBUG" ]]; then log "[DEBUG] Will use monitor ${FSMON_FSWATCH_MONITOR}"; fi
  fi

  log "[INFO] using fswatch"
  fswatch -f="%s"${FSWATCH_MONITOR} --event Created --event Updated --event Removed --event Renamed --event MovedFrom --event MovedTo --event IsFile --event-flag-separator=":" --format="%t,%f,%p" -r "${DIR_TO_MONITOR_FULL_PATH}" | while read EVENTINFO
  do
    if [[ ${FSMON_LOG_LEVEL} == "DEBUG" ]]; then log "[DEBUG] Received message from fswatch: ${EVENTINFO}"; fi
    sendMessage ${EVENTINFO}
  done
fi