// Import the necessary object from winston for logging
const {createLogger, format, transports} = require('winston');
const {combine, timestamp, printf} = format;

// Create a custom format for the log file
const myFormat = printf(({level, message, timestamp}) => {
    return `${timestamp} [${level}]: ${message}`;
});

// Now create the logger
const logger = createLogger({
    level: 'debug',
    format: combine(
        timestamp(),
        myFormat
    ),
    transports: [new transports.Console()]
});

// Grab the express extension to winston logging
var expressWinston = require('express-winston');

// Now grab the express module so we can spin up an HTTP server
const express = require('express');

// Import the body parser module
const bodyParser = require('body-parser');

// Create the express application server
const app = express();

// Assign a port that this server will eventually listen to
const port = 3000;

// Add the morgan logger to the express server
app.use(expressWinston.logger({
    winstonInstance: logger
}));

// Add the body parser
app.use(bodyParser.json());

// Add a route to handle get requests
app.get('/', (req, res) => res.send('Got your message'));
app.post('/', (req, res) => {
    logger.debug('Test server got message:');
    logger.debug('Timestamp: ' + req.body.timestamp);
    logger.debug('Event: ' + req.body.event);
    logger.debug('RepoName: ' + req.body.repoName);
    logger.debug('ResourceURL: ' + req.body.resourceURL);
    logger.debug('ResourceType: ' + req.body.resourceType);
    res.send('Got your message')
});

// Now start server and listen on a port
app.listen(port, () => console.log(`The Testing app listening on port ${port}!`));