# This is a Docker build file that starts with a base CentOS 7 image and creates a new
# image that installs several packages and utilities that can be used to monitor directories
# and will pipe notifications in different mechanisms.

# Start with the Centos base image
FROM centos:7

# Now run the commands to build up the image (all run as root)
RUN yum upgrade -y && \
   yum update -y

# Install the EPEL repo first
RUN yum install -y epel-release

# Set the working directory
WORKDIR /root

# Now install all the necessary tools
RUN yum install -y inotify-tools \
   make \
   nodejs \
   gcc \
   gcc-c++ \
   gmp-devel \
   mpfr-devel \
   libmpc-devel && \
   yum clean all && \
   curl https://ftp.gnu.org/gnu/gcc/gcc-8.2.0/gcc-8.2.0.tar.gz -O && \
   gunzip gcc-8.2.0.tar.gz && \
   tar -xvf gcc-8.2.0.tar && \
   mkdir gcc-8.2.0-build && \
   cd gcc-8.2.0-build && \
   ../gcc-8.2.0/configure --disable-multilib --enable-languages=c,c++ && \
   make -j $(nproc) && \
   make install && \
   cd .. && \
   curl -L https://github.com/emcrisostomo/fswatch/releases/download/1.14.0/fswatch-1.14.0.tar.gz -O && \
   gunzip fswatch-1.14.0.tar.gz && \
   tar -xvf fswatch-1.14.0.tar && \
   cd fswatch-1.14.0 && \
   PATH=/usr/local/bin:$PATH LD_LIBRARY_PATH=/usr/local/lib64:$LD_LIBRARY_PATH ./configure --disable-dependency-tracking && \
   PATH=/usr/local/bin:$PATH LD_LIBRARY_PATH=/usr/local/lib64:$LD_LIBRARY_PATH make && \
   PATH=/usr/local/bin:$PATH LD_LIBRARY_PATH=/usr/local/lib64:$LD_LIBRARY_PATH make install && \
   cd .. && \
   yum remove -y libmpc-devel \
   mpfr-devel \
   gmp-devel \
   gcc-c++ \
   gcc \
   make && \
   rm -f gcc-8.2.0.tar && \
   rm -rf gcc-8.2.0 && \
   rm -rf gcc-8.2.0-build && \
   rm -rf /usr/local/libexec/gcc && \
   rm -f fswatch-1.14.0.tar && \
   rm -rf fswatch-1.14.0 && \
   yum clean all

# This section installs the test server, the run script and creates the proper accounts and directories to
# run the application.  It is a separate RUN as the first section takes FOREVER to build so I wanted to not have
# to repeat that if something below did not work.
# TODO kgomes, get rid of the mkdir for /var/log/fsmon as this should be mounted from the host
RUN mkdir /var/log/fsmon && \
   mkdir /var/log/fsmon/test && \
   # Now let's create the user that will run the monitor
   groupadd fsmon && \
   useradd -g fsmon -m -s /bin/bash fsmon && \
   echo 'fsmonpass' | passwd fsmon --stdin && \
   mkdir /home/fsmon/fsmon-test && \
   chown fsmon:fsmon /home/fsmon/fsmon-test && \
   chown fsmon:fsmon /var/log/fsmon && \
   chown fsmon:fsmon /var/log/fsmon/test

# Add the test server and run script, then install the supporting modules
ADD ./test/package.json /home/fsmon/fsmon-test/package.json
ADD ./test/server.js /home/fsmon/fsmon-test/server.js
ADD ./run.sh /home/fsmon/run.sh
RUN chown fsmon:fsmon /home/fsmon/fsmon-test/package.json && \
   chown fsmon:fsmon /home/fsmon/fsmon-test/server.js && \
   chown fsmon:fsmon /home/fsmon/run.sh && \
   chmod a+x /home/fsmon/run.sh

# Switch to the new user
USER fsmon

# Set the working directory
WORKDIR /home/fsmon

# Install the test server
RUN cd /home/fsmon/fsmon-test && \
   npm install

# Run the monitor command
CMD "/home/fsmon/run.sh"