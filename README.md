# File System Monitor (fsmon)

This code repository contains files that will build a Docker image that can be used to monitor a directory and emit events when file systems events happen.  It will either use inotify or fswatch to do the monitoring depending on how it is configured when it is run.

Please consult the documentation in the 'mkdocs' directory.

If you want to see and/or develop the documentation for this project:

1. Install mkdocs
1. Install mkdocs-material
1. Check out this repo
1. cd into the mkdocs dir
1. Run ```mkdocs serve```
1. Point your browser to [http://localhost:8000](http://localhost:8000) to see the documentation. If you edit any files under the mkdocs directory, they will update in your browser after saving.
